from . import views
from django.urls import path

app_name = 'shop'

urlpatterns = [
    path('', views.index, name='index'),
    path('base/', views.base, name='base'),
    path('store/', views.store, name='store'),
    path('product/<int:pk>/', views.product, name='product'),

]
