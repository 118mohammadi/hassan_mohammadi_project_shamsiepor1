from django.contrib import admin
from . import models


class BaseAdmin(admin.ModelAdmin):
    list_display = ('name', 'create_time',)
class IndexAdmin(admin.ModelAdmin):
    list_display = ('name', 'create_time',)



admin.site.register(models.drbare_ma, BaseAdmin)
admin.site.register(models.logo, BaseAdmin)
admin.site.register(models.hspio, IndexAdmin)
admin.site.register(models.hspia, IndexAdmin)
